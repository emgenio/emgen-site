# coding=utf8
from imapclient import IMAPClient
from email.utils import parseaddr
import email
from .storage.helpers import find_address_and_delete
from .config import config
import requests

MAILGUN_URL = "https://api.mailgun.net/v3/emgen.it/messages"
API_KEY = config["mailgun"]["key"]

def do_forward(id_email, raw_data):
    id = parseaddr(str(id_email))[1].split('@')[0]
    deleted_object = find_address_and_delete(id=id)
    if deleted_object:

        msg = email.message_from_bytes(raw_data)

        for part in msg.walk():
            typ = part.get_content_type()
            if typ and typ.lower() == "text/plain":
                body = part.get_payload(decode=True)
                break

        data = {
            "to": [deleted_object['to']],
            "from": "Emgen Proxy <proxy@emgen.it>",
            "subject": msg["subject"],
            "html": body
        }

        result = requests.post(MAILGUN_URL, auth=("api", API_KEY), data=data)

        if "Thank you" in result.text:
            return True
    return False

def main():
    mail = IMAPClient(config['mail']['host'], use_uid=True)
    mail.login(config['mail']['user'], config['mail']['password'])
    mail.select_folder('INBOX')
    messages = mail.search(['ALL'])
    if len(messages) > 0:
        response = mail.fetch(messages, ['BODY.PEEK[HEADER.FIELDS (TO)]', 'RFC822'])
        for msgnum, data in response.items():
            do_forward(data[b'BODY[HEADER.FIELDS (TO)]'], data[b'RFC822'])
        mail.delete_messages(messages)
        mail.expunge()
        return True
    else:
        print('No messages')
        return False

if __name__ == '__main__':
    main()
